/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

import core.GameObject;
import core.ObjectId;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.LinkedList;
import main.Game;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lenovo
 */
public class ExitPointTest {
    
    public ExitPointTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

//    /**
//     * Test of tick method, of class ExitPoint.
//     */
//    @Test
//    public void testTick() {
//        System.out.println("tick");
//        LinkedList<GameObject> objects = null;
//        ExitPoint instance = null;
//        instance.tick(objects);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of render method, of class ExitPoint.
//     */
//    @Test
//    public void testRender() {
//        System.out.println("render");
//        Graphics2D g = null;
//        ExitPoint instance = null;
//        instance.render(g);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getBounds method, of class ExitPoint.
//     */
//    @Test
//    public void testGetBounds() {
//        System.out.println("getBounds");
//        ExitPoint instance = null;
//        Rectangle expResult = null;
//        Rectangle result = instance.getBounds();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of checkCollision method, of class ExitPoint.
     */
    @Test
    public void testCheckCollision_haveIntersection() {
        System.out.println("checkCollision");
        Game.currentState = Game.GameState.LEVEL_CANDY;
        Game.GameState expectedResult = Game.GameState.LEVEL_CHOCO;
        
        Player player;
        player = Player.getInstance();
        player.setX(10);
        player.setY(10);
        LinkedList<GameObject> objects = new LinkedList<GameObject>();
        objects.add(player);
        ExitPoint instance = new ExitPoint(ObjectId.TYPE_EXIT, 30, 30, 70, 70);
        instance.checkCollision(objects);
        Game.GameState actualResult = Game.currentState;
        
        assertEquals(expectedResult, actualResult);
    }
    
    public void testCheckCollision_dontHaveIntersection() {
        System.out.println("checkCollision");
        Game.currentState = Game.GameState.LEVEL_CANDY;
        Game.GameState expectedResult = Game.GameState.LEVEL_CANDY;
        
        Player player;
        player = Player.getInstance();
        player.setX(5);
        player.setY(5);
        LinkedList<GameObject> objects = new LinkedList<GameObject>();
        objects.add(player);
        ExitPoint instance = new ExitPoint(ObjectId.TYPE_EXIT, 30, 30, 70, 70);
        instance.checkCollision(objects);
        Game.GameState actualResult = Game.currentState;
        
        assertEquals(expectedResult, actualResult);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

import core.GameObject;
import core.ObjectId;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.LinkedList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lenovo
 */
public class CoinTest {
    
    public CoinTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of tick method, of class Coin.
     */
//    @Test
//    public void testTick() {
//        System.out.println("tick");
//        LinkedList<GameObject> objects = null;
//        Coin instance = null;
//        instance.tick(objects);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of render method, of class Coin.
     */
//    @Test
//    public void testRender() {
//        System.out.println("render");
//        Graphics2D g = null;
//        Coin instance = null;
//        instance.render(g);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getBounds method, of class Coin.
//     */
//    @Test
//    public void testGetBounds() {
//        System.out.println("getBounds");
//        Coin instance = null;
//        Rectangle expResult = null;
//        Rectangle result = instance.getBounds();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of checkCollision method, of class Coin.
     */
    @Test
    public void testCheckCollision() {
        System.out.println("checkCollision");
        Player player;
        player = Player.getInstance();
        player.setX(3);
        player.setY(3);
        LinkedList<GameObject> objects = new LinkedList<GameObject>();
        objects.add(player);
        Coin instance = new Coin(ObjectId.TYPE_COINS, 30,30,50, 50);
        int expectedResult = Player.GOLD + 100;
        instance.checkCollision(objects);
        int actualRes = Player.GOLD;
        assertEquals(expectedResult, actualRes);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

import core.GameObject;
import core.ObjectId;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.LinkedList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lenovo
 */
public class SpikeTest {
    
    public SpikeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of tick method, of class Spike.
     */
    @Test
    public void testTick() {
        System.out.println("tick");
        LinkedList<GameObject> objects = null;
        Spike instance = null;
        instance.tick(objects);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of render method, of class Spike.
     */
    @Test
    public void testRender() {
        System.out.println("render");
        Graphics2D g = null;
        Spike instance = null;
        instance.render(g);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getBounds method, of class Spike.
     */
    @Test
    public void testGetBounds() {
        System.out.println("getBounds");
        Spike instance = null;
        Rectangle expResult = null;
        Rectangle result = instance.getBounds();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of checkCollision method, of class Spike.
     */
    @Test
    public void testCheckCollision() {
        System.out.println("checkCollision");
        int expectedResult = 0;
        Player player;
        player = Player.getInstance();
        player.setX(5);
        player.setY(5);
        LinkedList<GameObject> objects = new LinkedList<GameObject>();
        objects.add(player);
        Spike instance = new Spike(ObjectId.Spike, 30,30,15, 15);
        
        instance.checkCollision(objects);
        int actualRes = Player.LIFE;
        assertEquals(expectedResult, actualRes);
    }
    
}
